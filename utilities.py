
import re
import pyautogui
import time
import serial

def importTestProfile(file):
    data = []
    with open(file, "r") as f:
        contents = f.read()
        matches = re.findall(
            r'<flush glucose="([\d.]+)" minutes="(\d+)"/>\s*<flow minutes="(\d+)"/>', contents)

        for match in matches:
            glucose = float(match[0])
            minutes = int(match[1])
            flow_minutes = int(match[2])
            data.append({
                "glucose": glucose,
                "minutes": minutes,
                "flow_minutes": flow_minutes
                })
            
    return data        
            
def setHPLC(A,B,C,D,Rate,debug):
    
    coordA=[262,458];
    #coordB=[337,457];
    coordB=[350,469];

    coordC=[413,469];
    coordD=[490,459];
    coordCompSend=[574,467];
    coordRate=[352,404];
    coordRateSend=[481,404];
   
    ## set %'s for input
    
    pyautogui.click(x=coordB[0], y=coordB[1], clicks=1, interval=1, button='left');
    if debug!='debug':
        pyautogui.press('backspace')
        pyautogui.press('backspace')
        pyautogui.press('backspace')
        pyautogui.typewrite(B, interval=0) ;
        
    pyautogui.press('tab');
    pyautogui.typewrite(C, interval=0) ;
    pyautogui.press('tab');
    pyautogui.press('tab');
    pyautogui.press('tab');
    pyautogui.press('enter');
    #pyautogui.click(x=coordA[0], y=coordA[1], clicks=1, interval=0, button='left');
    # if debug!='debug':
    #     pyautogui.typewrite(A, interval=0) ;
    # pyautogui.click(x=coordC[0], y=coordC[1], clicks=1, interval=0, button='left');
    # if debug!='debug':
    #     pyautogui.press('backspace')
    #     pyautogui.press('backspace')
    #     pyautogui.press('backspace')
    #     pyautogui.typewrite(C, interval=0) ;
    # pyautogui.click(x=coordD[0], y=coordD[1], clicks=1, interval=0, button='left');
    # if debug!='debug':
    #     pyautogui.typewrite(D, interval=0) ;
        
    pyautogui.click(x=coordCompSend[0], y=coordRateSend[1], clicks=1,interval=0, button='left');

    # set pump rate
    pyautogui.click(x=coordRate[0], y=coordRate[1], clicks=1,interval=0, button='left');
    pyautogui.press('backspace')
    pyautogui.press('backspace')
    pyautogui.press('backspace')
    pyautogui.press('backspace')
    pyautogui.typewrite(Rate, interval=0)
    pyautogui.press('tab');
    pyautogui.press('enter');
   # pyautogui.click(x=coordRateSend[0], y=coordRateSend[1], clicks=1,interval=0, button='left');
    print("HPLC % set to: Glucose-" +  str(A)+" %, Buffer-"+str(B)+" %, Int-" +str(C) )
    
def toggleHPLC(toggle):
#    coordPumpOn=[293,335];
    coordPumpOn=[293,339];

    coordPumpOff=[341,337];
    coordPumpStandby=[385,339];
    print("HPLC set to " + str(toggle))
    if toggle=='on':
        pyautogui.click(x=coordPumpOn[0], y=coordPumpOn[1], clicks=1, interval=0, button='left');
    elif toggle=='off':
        pyautogui.click(x=coordPumpOff[0], y=coordPumpOff[1], clicks=1, interval=0, button='left');
    elif toggle=='standby':
        pyautogui.click(x=coordPumpStandby[0], y=coordPumpStandby[1], clicks=1, interval=0, button='left');

def togglePP(toggle):
    #command must be H, L, or q
   ser = serial.Serial('COM5', 9600)
   stop='L'
   start='H'
   time.sleep(2)
   print('pump set to ' +str(toggle))
   if toggle=='on':
        byte_commandStart = start.encode(encoding='ascii')
        ser.write(byte_commandStart)   # send a byte
        time.sleep(0.5)
        
   elif toggle=='off':
        byte_commandStop=stop.encode(encoding='ascii')
        ser.write(byte_commandStop)   # send a byte
        time.sleep(0.5)


  
       