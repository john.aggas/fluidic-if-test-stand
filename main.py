import pyautogui
import time
import serial
import utilities
glucoseStockConc=650
flushRate=3 #mL/min
interferent=0
fillTime=75
wasteTime=25
testProfile="C:\\Users\\testdev\\Documents\\PythonScripts\\pyramidTest2.prg"
#testProfile="C:\\Users\\aggasj\\Documents\\Python Scripts\\test2.prg"

data=utilities.importTestProfile(testProfile)


for i in range(len(data)):
    glucose=data[i]['glucose']
    flushMins=data[i]['minutes']
    flowMins=data[i]['flow_minutes']

    
    glucosePct=round(glucose/glucoseStockConc*100,1)
    if interferent==0:
        bufferPct=100-glucosePct
            #intfPct=  NEED TO CALC FOR INT
    print("please minimize spyder");
    time.sleep(3);
  #  utilities.togglePP('on')
   # time.sleep(wasteTime)
    utilities.togglePP('off')
    print("beginning step "+ str(i+1) + ": " + str(glucose) + " mg/dL glucose") ;       
    utilities.setHPLC(str(bufferPct),str(glucosePct),'0','0',str(flushRate),'n');
    utilities.toggleHPLC('on')
    time.sleep(fillTime) #fill and flush 2 mins
    utilities.togglePP('on')
    utilities.toggleHPLC('standby')
    time.sleep(wasteTime) #fill and flush 2 mins
    utilities.togglePP('off')
    utilities.toggleHPLC('on')
    time.sleep(fillTime) #fill and flush 2 mins
    utilities.togglePP('on')
    utilities.toggleHPLC('standby')
    time.sleep(wasteTime) #fill and flush 2 mins
    utilities.togglePP('off')
    utilities.toggleHPLC('on')
    time.sleep(fillTime) #fill and flush 2 mins
    utilities.toggleHPLC('standby')
    time.sleep(flowMins*60)



utilities.toggleHPLC('off')









